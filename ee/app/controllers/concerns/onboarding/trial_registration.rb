# frozen_string_literal: true

module Onboarding
  class TrialRegistration
    def self.redirect_to_company_form?
      true
    end
  end
end
